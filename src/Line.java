public class Line implements Shape {
    private LegacyLine adapter = new LegacyLine();

    public void draw(int x1, int y1, int x2, int y2) {
        adapter.draw(x1, y1, x2, y2);
    }
}