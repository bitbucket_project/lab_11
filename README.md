## Adapter (wzorzec projektowy)

**Adapter** -  strukturalny wzorzec projektowy, kt�rego celem jest umo�liwienie wsp�pracy dw�m klasom o niekompatybilnych interfejsach. Adapter przekszta�ca interfejs jednej z klas na interfejs drugiej klasy. Innym zadaniem omawianego wzorca jest opakowanie istniej�cego interfejsu w nowy.

## Problem

Wzorzec adaptera stosowany jest najcz�ciej w przypadku, gdy wykorzystanie istniej�cej klasy jest niemo�liwe ze wzgl�du na jej niekompatybilny interfejs. Drugim powodem u�ycia mo�e by� ch�� stworzenia klasy, kt�ra b�dzie wsp�pracowa�a z klasami o nieokreslonych interfejsach.

## Struktura


Istnieja dwa warianty wzorca Adapter:

* klasowy,
* obiektowy,


R�nia si� one nieznacznie budowa oraz w�asciwosciami. Do stworzenia adaptera klasowego wykorzystywane jest wielokrotne dziedziczenie. Klasa adaptera dziedziczy prywatnie po klasie adaptowanej oraz publicznie implementuje interfejs klienta. W przypadku tego adaptera wywo�anie funkcji jest przekierowywane do bazowej klasy adaptowanej.
W przypadku adaptera obiektowego klasa adaptera dziedziczy interfejs, kt�rym pos�uguje si� klient oraz zawiera w sobie klas� adaptowan�. Rozwi�zanie takie umo�liwia oddzielenie klasy klienta od klasy adaptowanej. Komplikuje to proces przekazywania ��dania: klient wysy�a je do adaptera wywo�uj�c jedn� z jego metod. Nast�pnie adapter konwertuje wywo�anie na jedno b�d� kilka wywo�a� i kieruje je do obiektu/obiekt�w adaptowanych. Te przekazuj� wyniki dzia�ania bezpo�rednio do klienta


## Adapter dwukierunkowy

Zadaniem adaptera dwukierunkowego jest adaptowanie interfejs�w klienta oraz adaptowanego. Dzi�ki takiemu rozwi�zaniu ka�da z klas mo�e pe�ni� zar�wno funkcj� klienta jak i adaptowanego. Ten typ adaptera mo�na zaimplementowa� tylko za pomoc� wielokrotnego dziedziczenia

## Konsekwencje stosowania

Konsekwencje stosowania wzorca s� r�ne w zale�nosci od tego, z jakim typem mamy do czynienia. W przypadku typu klasowego s� to:

* brak mo�liwosci adaptowania klasy wraz z jej podklasami,
* mo�liwos� prze�adowania metod obiektu adaptowanego.

Do konsekwencji stosowania adaptera obiektowego nale��:

* mo�liwos� adaptacji klasy wraz z jej podklasami (zwi�zane jest to z wykorzystaniem sk�adania obiekt�w),
* mo�liwos� dodawania nowej funkcjonalnosci,
brak mo�liwosci prze�adowania metod obiektu adaptowanego.
W obu przypadkach nale�y liczy� si� z narzutem wydajnosciowym w tym wi�kszym, im wi�ksza jest niekompatybilnos� interfejs�w.


�r�d�o [Wikipedia](https://pl.wikipedia.org/wiki/Adapter_(wzorzec_projektowy)
